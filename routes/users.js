const express = require('express');
const router = express.Router();
const readData = require('../utils/readData');
const writeData = require('../utils/writeData');

/* GET users listing. */
router.get('/', (req, res, next) => {
  try {
    const users = JSON.parse(readData());
    if (users) {
      res.statusCode = 200;
      res.send(users);
    } else {
      res.statusCode = 500;
      res.end('Internal error');
    }
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end('Internal error');
  }

});

/* GET user by ID. */
router.get('/:id', (req, res, next) => {
  if (isNaN(Number(req.params.id))) {
    res.statusCode = 400;
    res.end('Bad request');
    return;
  }
  try {
    const users = JSON.parse(readData());
    const user = users.find((d) => d.id === +req.params.id);
    if (user) {
      res.statusCode = 200;
      res.send(user);
    } else {
      res.statusCode = 404;
      res.end('Not found');
    }
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end('Internal error');
  }
});

/* POST create user. */
router.post('/', (req, res, next) => {
  if (!req.body || !req.body.first_name || !req.body.last_name || !req.body.email) {
    res.statusCode = 400;
    res.end('Bad request');
    return;
  }
  try {
    const users = JSON.parse(readData());
    const newUser = Object.assign({}, {
      id: new Date().getTime(),
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
    });
    users.push(newUser);
    writeData(JSON.stringify(users));
    res.statusCode = 200;
    res.send(newUser);
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end('Internal error');
  }
});

/* PUT update user. */
router.put('/:id', (req, res, next) => {
  if (isNaN(Number(req.params.id)) || !req.body || !req.body.first_name || !req.body.last_name || !req.body.email) {
    res.statusCode = 400;
    res.end('Bad request');
    return;
  }
  try {
    let users = JSON.parse(readData());
    const userId = Number(req.params.id);
    users = users.map((u) => {
      return {
        id: u.id,
        first_name: u.id === userId ? req.body.first_name : u.first_name,
        last_name: u.id === userId ? req.body.last_name : u.last_name,
        email: u.id === userId ? req.body.email : u.email
      }
    });
    const user = users.find((u) => u.id === userId);
    writeData(JSON.stringify(users));
    res.statusCode = 200;
    res.send(user);
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end('Internal error');
  }
});

/* DELETE delete user by ID. */
router.delete('/:id', (req, res, next) => {
  if (isNaN(Number(req.params.id))) {
    res.statusCode = 400;
    res.end('Bad request');
    return;
  }
  try {
    let users = JSON.parse(readData());
    const userId = Number(req.params.id);
    users = users.filter((u) => u.id !== userId);
    writeData(JSON.stringify(users));
    res.statusCode = 200;
    res.send(`User with id: ${userId} deleted`);
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end('Internal error');
  }
});

module.exports = router;
