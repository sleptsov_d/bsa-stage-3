const fs = require('fs');
const path = require('path');

const readData = () => {
    const users = fs.readFileSync(path.join(__dirname, '/data.json'), {
        encoding: 'utf8'
    });
    return users;
};

module.exports = readData;