const fs = require('fs');
const path = require('path');

const writeData = (data) => {
    fs.writeFileSync(path.join(__dirname, '/data.json'), data, {
        encoding: 'utf8'
    });
};

module.exports = writeData;